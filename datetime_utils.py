import datetime
import pytz


UTC = pytz.UTC
CST = pytz.timezone("US/Central")
AM = "am"
PM = "pm"
AM_12 = datetime.time(hour=0)
PM_12 = datetime.time(hour=12)


########################################################################################################################
class DatetimeHandlingException(Exception):
    pass


########################################################################################################################
def convert_datetimes_to_utc(*args):
    return [UTC.localize(datetime_obj) for datetime_obj in args]
    # utc_start = UTC.localize(start_datetime)
    # utc_end = UTC.localize(end_datetime)
    # return utc_start, utc_end


########################################################################################################################
def get_12_am_date(datetime_obj):
    return datetime_obj.replace(hour=0, minute=0, second=0, microsecond=0)


########################################################################################################################
def cst_time(datetime_obj=datetime.datetime.now(tz=UTC)):
    cst = datetime_obj.astimezone(CST)
    return cst


########################################################################################################################
def get_meridiem(datetime_obj):
    if datetime_obj.hour >= 12:
        return PM
    else:
        return AM


########################################################################################################################
def get_hour_minute_and_meridiem(datetime_obj):
    meridiem = get_meridiem(datetime_obj)
    if meridiem == PM:
        hour = datetime_obj.hour - 12
    else:
        hour = datetime_obj.hour
    return hour, datetime_obj.minute, meridiem
